#!/usr/bin/python3

import sys
import json
import urllib.request

applist = [ (app['id'], app['releases'][0]['version'], app['releases'][0]['download']) for app in json.loads(urllib.request.urlopen('https://apps.nextcloud.com/api/v1/platform/%s/apps.json'%(sys.argv[1],)).read().decode('utf-8')) if app['id'] in ['mail', 'calendar', 'contacts', 'tasks', 'keeweb'] ]

for app in applist:
    print("%08s : %05s : %s"%app)

