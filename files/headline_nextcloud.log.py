#!/usr/bin/python3

import sys
import json

print("{}".format(len(sys.argv)))

input_file = sys.stdin

if ( (len(sys.argv) > 1) and (sys.argv[1] != "-") ):
    input_file = open(sys.argv[1])


jblocks = [ json.loads(input_block) for input_block in input_file ]

for block in jblocks:
    print("\n")
    for (k, v) in block.items():
        if ( k in ['version', 'userAgent', 'reqId', 'method'] ):
            continue

        if ( k == "message" ):
            if ( isinstance(v, str) ):
                print("  {:<11}: {}".format(k, v))
            elif ( isinstance(v, dict) ):
                print("message...")
                for (mk, mv) in v.items():
                    if ( isinstance(mv, str) or isinstance(mv, int)   ):
                        if ( mk in ['CustomMessage', 'Trace', 'Previous'] ):
                            continue
                        else:
                            print("  {:<11}: {}".format(mk, mv))
                    elif ( isinstance(mv, dict) ):
                        for (tk, tv) in mv.items():
                            if ( mk in ['CustomMessage', 'Trace', 'Previous'] ):
                                continue
                            print("    {:<11}: {}".format(tk, repr(tv)))
                    else:
                        print("  {:<11}: ...".format(mk))
            else:
                sys.exit(type(v))
        else:
            print("{:<11}: {}".format(k, repr(v)))
